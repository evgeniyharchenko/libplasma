# Translation of libplasma6 to Norwegian Bokmål
#
# Bjørn Steensrud <bjornst@skogkatt.homelinux.org>, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: plasmapkg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-31 00:40+0000\n"
"PO-Revision-Date: 2024-03-09 12:43+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Bokmål <l10n-no@lister.huftis.org>\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: declarativeimports/plasmaextracomponents/qml/BasicPlasmoidHeading.qml:81
#, kde-format
msgid "More actions"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:517
#, kde-format
msgctxt "@action:button"
msgid "Collapse"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/ExpandableListItem.qml:517
#, kde-format
msgctxt "@action:button"
msgid "Expand"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/PasswordField.qml:49
#, kde-format
msgid "Password"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:60
#, kde-format
msgid "Search…"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:62
#, kde-format
msgid "Search"
msgstr ""

#: declarativeimports/plasmaextracomponents/qml/SearchField.qml:73
#, kde-format
msgid "Clear search"
msgstr ""

#: plasma/applet.cpp:296
#, kde-format
msgid "Unknown"
msgstr "Ukjent"

#: plasma/applet.cpp:728
#, kde-format
msgid "Activate %1 Widget"
msgstr "Aktiver elementet %1"

#: plasma/containment.cpp:97 plasma/private/applet_p.cpp:99
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Remove %1"
msgstr ""

#: plasma/containment.cpp:103 plasma/corona.cpp:360 plasma/corona.cpp:478
#, kde-format
msgid "Enter Edit Mode"
msgstr ""

#: plasma/containment.cpp:106 plasma/private/applet_p.cpp:104
#, kde-format
msgctxt "%1 is the name of the applet"
msgid "Configure %1..."
msgstr "Sett opp «%1» …"

#: plasma/corona.cpp:309 plasma/corona.cpp:463
#, kde-format
msgid "Lock Widgets"
msgstr "Lås elementer"

#: plasma/corona.cpp:309
#, kde-format
msgid "Unlock Widgets"
msgstr "Lås opp elementer"

#: plasma/corona.cpp:358
#, kde-format
msgid "Exit Edit Mode"
msgstr ""

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:9
#, kde-format
msgid "Whether or not to create an on-disk cache for the theme."
msgstr "Om et mellomlager på disk skal opprettes for temaet."

#. i18n: ectx: label, entry, group (CachePolicies)
#: plasma/data/kconfigxt/libplasma-theme-global.kcfg:14
#, kde-format
msgid ""
"The maximum size of the on-disk Theme cache in kilobytes. Note that these "
"files are sparse files, so the maximum size may not be used. Setting a "
"larger size is therefore often quite safe."
msgstr ""
"Maksimal størrelse for tema-mellomlager på disk i kilobyte. Merk at disse "
"filene har mye tomrom, så maksimal størrelse blir kanskje ikke brukt. Det er "
"derfor ofte ganske trygt å oppgi stor størrelse."

#: plasma/private/applet_p.cpp:117
#, kde-format
msgid "Show Alternatives..."
msgstr ""

#: plasma/private/applet_p.cpp:228
#, kde-format
msgid "Widget Removed"
msgstr "Skjermelement fjernet"

#: plasma/private/applet_p.cpp:229
#, kde-format
msgid "The widget \"%1\" has been removed."
msgstr "Elementet «%1» er tatt bort."

#: plasma/private/applet_p.cpp:233
#, kde-format
msgid "Panel Removed"
msgstr "Panel fjernet"

#: plasma/private/applet_p.cpp:234
#, kde-format
msgid "A panel has been removed."
msgstr "Et panel er tatt bort."

#: plasma/private/applet_p.cpp:237
#, kde-format
msgid "Desktop Removed"
msgstr "Skrivebord tatt bort"

#: plasma/private/applet_p.cpp:238
#, kde-format
msgid "A desktop has been removed."
msgstr "Et skrivebord er tatt bort."

#: plasma/private/applet_p.cpp:241
#, kde-format
msgid "Undo"
msgstr "Angre"

#: plasma/private/applet_p.cpp:332
#, kde-format
msgid "Widget Settings"
msgstr "Elementinnstillinger"

#: plasma/private/applet_p.cpp:339
#, kde-format
msgid "Remove this Widget"
msgstr "Fjern dette elementet"

#: plasma/private/containment_p.cpp:58
#, kde-format
msgid "Remove this Panel"
msgstr "Fjern dette panelet"

#: plasma/private/containment_p.cpp:60
#, kde-format
msgid "Remove this Activity"
msgstr "Fjern denne aktiviteten"

#: plasma/private/containment_p.cpp:66
#, kde-format
msgid "Activity Settings"
msgstr "Aktivitetsinnstillinger"

#: plasma/private/containment_p.cpp:78
#, kde-format
msgid "Add Widgets..."
msgstr "Legg til skjermelementer …"

#: plasma/private/containment_p.cpp:197
#, kde-format
msgid "Could not find requested component: %1"
msgstr "Fant ikke den etterspurte komponenten: %1"

#: plasmaquick/appletquickitem.cpp:512
#, kde-format
msgid "The root item of %1 must be of type ContainmentItem"
msgstr ""

#: plasmaquick/appletquickitem.cpp:517
#, kde-format
msgid "The root item of %1 must be of type PlasmoidItem"
msgstr ""

#: plasmaquick/appletquickitem.cpp:525
#, kde-format
msgid "Unknown Applet"
msgstr ""

#: plasmaquick/appletquickitem.cpp:539
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""

#: plasmaquick/appletquickitem.cpp:542 plasmaquick/appletquickitem.cpp:549
#: plasmaquick/appletquickitem.cpp:555
#, kde-format
msgid "%1 is not compatible with Plasma %2"
msgstr ""

#: plasmaquick/appletquickitem.cpp:546
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""

#: plasmaquick/appletquickitem.cpp:552
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""

#: plasmaquick/appletquickitem.cpp:561 plasmaquick/appletquickitem.cpp:583
#, kde-format
msgid "Sorry! There was an error loading %1."
msgstr ""

#: plasmaquick/appletquickitem.cpp:578
#, kde-format
msgid "Error loading QML file: %1 %2"
msgstr ""

#: plasmaquick/appletquickitem.cpp:581
#, fuzzy, kde-format
#| msgid "Error loading Applet: package inexistent. %1"
msgid "Error loading Applet: package %1 does not exist."
msgstr "Feil ved lasting av miniprogram: pakke finnes ikke: %1"

#: plasmaquick/configview.cpp:231
#, fuzzy, kde-format
#| msgid "%1 Settings"
msgid "%1 — %2 Settings"
msgstr "%1-innstillinger"

#: plasmaquick/configview.cpp:232
#, kde-format
msgid "%1 Settings"
msgstr "%1-innstillinger"

#: plasmaquick/plasmoid/containmentitem.cpp:543
#, kde-format
msgid "Plasma Package"
msgstr ""

#: plasmaquick/plasmoid/containmentitem.cpp:547
#, kde-format
msgid "Install"
msgstr "Installer"

#: plasmaquick/plasmoid/containmentitem.cpp:558
#, kde-format
msgid "Package Installation Failed"
msgstr ""

#: plasmaquick/plasmoid/containmentitem.cpp:574
#, kde-format
msgid "The package you just dropped is invalid."
msgstr ""

#: plasmaquick/plasmoid/containmentitem.cpp:583
#: plasmaquick/plasmoid/containmentitem.cpp:652
#, kde-format
msgid "Widgets"
msgstr "Elementer"

#: plasmaquick/plasmoid/containmentitem.cpp:588
#, kde-format
msgctxt "Add widget"
msgid "Add %1"
msgstr ""

#: plasmaquick/plasmoid/containmentitem.cpp:602
#: plasmaquick/plasmoid/containmentitem.cpp:656
#, kde-format
msgctxt "Add icon widget"
msgid "Add Icon"
msgstr ""

#: plasmaquick/plasmoid/containmentitem.cpp:614
#, kde-format
msgid "Wallpaper"
msgstr "Bakgrunnsbilde"

#: plasmaquick/plasmoid/containmentitem.cpp:624
#, kde-format
msgctxt "Set wallpaper"
msgid "Set %1"
msgstr ""

#: plasmaquick/plasmoid/dropmenu.cpp:28
#, kde-format
msgid "Content dropped"
msgstr ""
